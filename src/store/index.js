import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./rootSlice.js";
import modalReducer from "./modal_slices.js";

export default configureStore({
  reducer: { root: rootReducer, modals: modalReducer },
});
