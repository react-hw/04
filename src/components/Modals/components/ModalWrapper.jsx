import React from "react";
import PropTypes from "prop-types";

import "./ModalBase.scss";

const ModalWrapper = ({ children, isOpen, onClick }) => {
  const onCloseModalVeil = (event) => {
    if (event.target.classList.contains("madal__wrapper")) {
      onClick();
    }
  };

  return (
    <>
      {isOpen && (
        <div className="madal__wrapper" onClick={onCloseModalVeil}>
          {children}
        </div>
      )}
    </>
  );
};

ModalWrapper.defaultProps = {
  isOpen: false,
};

ModalWrapper.propTypes = {
  isOpen: PropTypes.bool,
  onClick: PropTypes.func,
  children: PropTypes.any,
};

export default ModalWrapper;
