import React from "react";
import PropTypes from "prop-types";

import "./ModalBase.scss";

const ModalFooter = ({ children }) => {
  return <div className="modal__footer">{children}</div>;
};

ModalFooter.propTypes = {
  children: PropTypes.any,
};

export default ModalFooter;
