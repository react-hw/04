import ModalAddProduct from "./ModalAddProduct";
import ModalDelCart from "./ModalDelCart";
import ModalDelFavorite from "./ModalDelFavorite";

export { ModalAddProduct, ModalDelCart, ModalDelFavorite };
