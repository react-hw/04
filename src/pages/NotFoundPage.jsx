import React from "react";

import "./components/Pages.scss";

const NotFoundPage = () => {
  return (
    <div className="pages__not-found wrapper">
      <p className="pages__not-found code">404</p>
      <p className="pages__not-found text">PAGE NOT FOUND</p>
    </div>
  );
};

export default NotFoundPage;
