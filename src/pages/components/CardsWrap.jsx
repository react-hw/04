import React from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import "./Pages.scss";

const CardsWrap = ({ className, children }) => {
  return <div className={cn("cards-wrap", className)}>{children}</div>;
};

CardsWrap.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
};

export default CardsWrap;
