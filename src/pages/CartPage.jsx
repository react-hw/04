import React from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";

import Pages, { CardsWrap } from "./components";
import { CardToCart } from "../components/Cards";
import ButtonClassic from "../components/Buttons";
const CartPage = (props) => {
  const { carts, handleModalDelCart, onCurrentProduct, handleFavorite } = props;
  const navigate = useNavigate();

  const handleNavigate = () => {
    navigate("/order");
  };

  return (
    <Pages>
      <div className="pages__cart wrapper">
        <ButtonClassic className={"btn__to_cart"} onClick={handleNavigate}>
          TO ORDER
        </ButtonClassic>
        <CardsWrap>
          {carts.map((product) => {
            return (
              <CardToCart
                key={product.vendorCode}
                product={product}
                onModalDelCart={() => {
                  handleModalDelCart();
                  onCurrentProduct(product);
                }}
                handleFavorite={handleFavorite}
              />
            );
          })}
        </CardsWrap>
      </div>
    </Pages>
  );
};

CartPage.propTypes = {
  carts: PropTypes.array,
  onCurrentProduct: PropTypes.func,
  handleModalDelCart: PropTypes.func,
  handleFavorite: PropTypes.func,
};

export default CartPage;
