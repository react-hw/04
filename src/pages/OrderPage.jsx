import React from "react";

import "./OrderPage.scss";

const OrderPage = () => {
  return (
    <form className="form">
      <fieldset className="fieldset-outer">
        <fieldset className="fieldset-inner">
          {/* <legend>First Name*</legend> */}
          <label htmlFor="firstname">First Name*</label>
          <input type="text" placeholder="First Name" id="firstname" />
        </fieldset>
        <fieldset className="fieldset-inner">
          <label htmlFor="lastname">Last Name*</label>
          <input type="text" placeholder="Last Name" id="lastname" />
        </fieldset>
      </fieldset>

      <fieldset className="fieldset-outer">
        <fieldset className="fieldset-inner">
          <label htmlFor="firstname">First Name*</label>
          <input type="text" placeholder="First Name" id="firstname" />
        </fieldset>
        <fieldset className="fieldset-inner">
          <label htmlFor="lastname">Last Name*</label>
          <input type="text" placeholder="Last Name" id="lastname" />
        </fieldset>
      </fieldset>
    </form>
  );
};

export default OrderPage;
